﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PoiController : MonoBehaviour
{
    GameObject fish;
    private Vector3 position;
    
    int flag = 0;
    int missCount = 0;
    int limit = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        // Debug.Log(Input.mousePosition);

        position = Input.mousePosition;
        position.z = 10f;
        gameObject.transform.position = Camera.main.ScreenToWorldPoint(position);

        //クリックしたら小さくなる
        if (Input.GetMouseButtonDown(0))
        {
            transform.localScale = new Vector3(0.8f, 0.8f, 0);

            if (flag == 1)
            {
                Debug.Log("すくった");
                Destroy(fish);

                GameObject director = GameObject.Find("GameDirector");
                director.GetComponent<GameDirector>().IncreaseCo();
            }
            else
            {
                missCount++;
                if (missCount == limit)
                {
                    SceneManager.LoadScene("GameoverScene");
                    Debug.Log("お手付き");
                }
            }
        }

        //離したら戻す
        if (Input.GetMouseButtonUp(0))
        {
            transform.localScale = new Vector3(1, 1, 0);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Fish")
        {
            Debug.Log("ふれた：" + collision.gameObject.name);
            flag = 1;
            this.fish = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Fish")
        {
            Debug.Log("はなれた：" + collision.gameObject.name);
            flag = 0;
        }
    }
}
