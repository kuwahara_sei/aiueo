﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldFishGenerator : MonoBehaviour
{
  
    public GameObject GoldFishPrefab;
    float span = 2.0f;
    float delta = 0;


    int ID = 0;


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject fish = Instantiate(GoldFishPrefab) as GameObject;
            int p = Random.Range(-4, 4);
            int x = Random.Range(-9, 9);
            fish.transform.position = new Vector3(x, p, 0);
            fish.gameObject.name = "金魚" + ID;
            ID++;
        }
    }
}
