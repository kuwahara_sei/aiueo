﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    public int count = 0;
    public GameObject counter = null;

    // Start is called before the first frame update
    void Start()
    {
        this.counter = GameObject.Find("Counter");
    }

    public void IncreaseCo()
    {
        count++;
    }

    // Update is called once per frame
    void Update()
    {
        Text fishText = this.counter.GetComponent<Text>();
        fishText.text = count + "匹目";
    }
}
